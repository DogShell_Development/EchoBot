import { expect } from 'chai';
import * as sinon from 'sinon';

import { Message, User, Collection } from 'discord.js';

import { isBotMessage, isAdmin, roleIds } from '../src/helpers/helpers';


describe('Helper Functions', () => {
    it('Should return true on "isBotMessage" if message is sent by bot', () => {
        const myMessage = {
            author: {
                id: '420422150726615040'
            }
        };
        expect(isBotMessage(myMessage)).to.be.true;
    });

    it('Should return true on "isAdmin" if message is sent by an admin', () => {
        const myRoles = new Collection();
        myRoles.set('id', roleIds.Admin_Dev)
        const myMessage = {
            member: {
                roles: {
                    exists: (key, val) => {
                        if (key === 'id' && val === roleIds.Admin) {
                            return true;
                        }
                    }
                }
            }
        };
        expect(isAdmin(myMessage)).to.be.true;
    })
})