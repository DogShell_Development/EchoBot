import { expect } from 'chai';
import * as sinon from 'sinon';

import { Client, Message } from 'discord.js';

import { Admin } from '../src/modules/admin';
import { roleIds } from '../src/helpers/helpers';
import { Uptime } from '../src/helpers/uptime';


describe('Admin', () => {
    it('should exist', () => {
        const myClient = new Client();
        const myUptime = new Uptime();
        const myAdmin = new Admin(myClient, myUptime);
        expect(myAdmin).to.exist;
    });

    it('should send an uptime response on !uptime message', () => {
        const myClient = new Client();
        const myUptime = new Uptime();
        const msgSendSpy = sinon.spy();
        const mockResponse = (str: any, message: any) => {
            message({
                channel: {
                    send: (str: string) => { msgSendSpy(str); }
                },
                content: '!uptime',
                member: {
                    roles: {
                        exists: () => { return true; }
                    }
                }
            });
        };
        const mock = sinon.stub(myClient, 'on').callsFake(mockResponse);
        const myAdmin = new Admin(myClient, myUptime);
        expect(msgSendSpy.calledOnce).to.be.true;
        expect(msgSendSpy.args[0][0]).to.contain('__Echobot Current Stats');
    })

    it('should add Mute role on !mute <user>', () => {
        const myClient = new Client();
        const myUptime = new Uptime();
        const addRoleSpy = sinon.spy();
        const mockResponse = (str: any, message: any) => {
            message({
                content: '!mute <@12345>',
                guild: {
                    fetchMember: () => {
                        return new Promise((resolve) => {
                            const resMember =
                                resolve({
                                    addRole: (roleId, reason) => {
                                        addRoleSpy(roleId, reason)
                                    }
                                });
                        })
                    }
                },
                member: {
                    displayName: 'Test Admin',
                    roles: {
                        exists: (key, val) => {
                            return true // pass admin validation for testing
                        }
                    }
                }
            });
        };
        const mock = sinon.stub(myClient, 'on').callsFake(mockResponse);
        const myAdmin = new Admin(myClient, myUptime);
        // Use promise w/ timeout to ensure method is called
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, 1000);
        }).then(() => {
            expect(addRoleSpy.calledOnce).to.be.true;
            expect(addRoleSpy.args[0][0]).to.contain(roleIds.ChatMute);
            expect(addRoleSpy.args[0][1]).to.contain('User chat-muted by Test Admin');
        })

    })

    it('should remove Mute role on !unmute <user>', () => {
        const myClient = new Client();
        const myUptime = new Uptime();
        const removeRoleSpy = sinon.spy();
        const mockResponse = (str: any, message: any) => {
            message({
                content: '!unmute <@12345>',
                guild: {
                    fetchMember: () => {
                        return new Promise((resolve) => {
                            const resMember =
                                resolve({
                                    removeRole: (roleId, reason) => {
                                        removeRoleSpy(roleId, reason)
                                    }
                                });
                        })
                    }
                },
                member: {
                    displayName: 'Test Admin',
                    roles: {
                        exists: (key, val) => {
                            return true // pass admin validation for testing
                        }
                    }
                }
            });
        };
        const mock = sinon.stub(myClient, 'on').callsFake(mockResponse);
        const myAdmin = new Admin(myClient, myUptime);
        // Use promise w/ timeout to ensure method is called
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, 1000);
        }).then(() => {
            expect(removeRoleSpy.calledOnce).to.be.true;
            expect(removeRoleSpy.args[0][0]).to.contain(roleIds.ChatMute);
            expect(removeRoleSpy.args[0][1]).to.contain('User chat-unmuted by Test Admin');
        })

    })

    it('should add BotMute role on !botmute <user>', () => {
        const myClient = new Client();
        const myUptime = new Uptime();
        const addRoleSpy = sinon.spy();
        const mockResponse = (str: any, message: any) => {
            message({
                content: '!botmute <@12345>',
                guild: {
                    fetchMember: () => {
                        return new Promise((resolve) => {
                            const resMember =
                                resolve({
                                    addRole: (roleId, reason) => {
                                        addRoleSpy(roleId, reason)
                                    }
                                });
                        })
                    }
                },
                member: {
                    displayName: 'Test Admin',
                    roles: {
                        exists: (key, val) => {
                            return true // pass admin validation for testing
                        }
                    }
                }
            });
        };
        const mock = sinon.stub(myClient, 'on').callsFake(mockResponse);
        const myAdmin = new Admin(myClient, myUptime);
        // Use promise w/ timeout to ensure method is called
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, 1000);
        }).then(() => {
            expect(addRoleSpy.calledOnce).to.be.true;
            expect(addRoleSpy.args[0][0]).to.contain(roleIds.BotMute);
            expect(addRoleSpy.args[0][1]).to.contain('User bot-muted by Test Admin');
        })

    })

    it('should remove BotMute role on !unbotmute <user>', () => {
        const myClient = new Client();
        const myUptime = new Uptime();
        const removeRoleSpy = sinon.spy();
        const mockResponse = (str: any, message: any) => {
            message({
                content: '!unbotmute <@12345>',
                guild: {
                    fetchMember: () => {
                        return new Promise((resolve) => {
                            const resMember =
                                resolve({
                                    removeRole: (roleId, reason) => {
                                        removeRoleSpy(roleId, reason)
                                    }
                                });
                        })
                    }
                },
                member: {
                    displayName: 'Test Admin',
                    roles: {
                        exists: (key, val) => {
                            return true // pass admin validation for testing
                        }
                    }
                }
            });
        };
        const mock = sinon.stub(myClient, 'on').callsFake(mockResponse);
        const myAdmin = new Admin(myClient, myUptime);
        // Use promise w/ timeout to ensure method is called
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, 1000);
        }).then(() => {
            expect(removeRoleSpy.calledOnce).to.be.true;
            expect(removeRoleSpy.args[0][0]).to.contain(roleIds.BotMute);
            expect(removeRoleSpy.args[0][1]).to.contain('User bot-unmuted by Test Admin');
        })

    })
});