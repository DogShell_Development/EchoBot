import { expect } from 'chai';

import { Client } from 'discord.js';

import { Logger } from '../src/modules/logger';


describe('Logger', () => {
    const myClient = new Client();

    it('should exist', () => {
        const myLogger = new Logger(myClient);
        expect(myLogger).to.exist;
    });    
})