import { expect } from 'chai';

import { Client } from 'discord.js';

import { FAQ } from '../src/modules/faq';


describe('FAQ', () => {
    const myClient = new Client();

    it('should exist', () => {
        const myFAQ = new FAQ(myClient);
        expect(myFAQ).to.exist;
    });    
})