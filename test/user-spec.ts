import { expect } from 'chai';
import * as sinon from 'sinon';

import { Client } from 'discord.js';
import { Connection } from 'mysql';

import { User } from '../src/modules/user';


describe('User', () => {
    const myClient = new Client();
    let myDb: Connection;

    it('should exist', () => {
        const myUser = new User(myClient, myDb);
        expect(myUser).to.exist;
    });

    it('should delete any message that fails profanity checks and notify author', () => {
        const myClient = new Client();
        const msgDelete = sinon.spy();
        const notifyAuthor = sinon.spy();
        const mockResponse = (str: any, message: any) => {
            message({
                author: {
                    username: 'Test',
                    createDM: () => {
                        return new Promise((resolve, reject) => {
                            const channel = {
                                send: (msg) => {
                                    notifyAuthor(msg);
                                }
                            };
                            resolve(channel);
                        })
                    }
                },
                delete: () => {
                    msgDelete();
                    return new Promise((resolve) => {
                        resolve();
                    })
                },
                content: 'retard',
                member: {
                    roles: {
                        exists: () => { return true; }
                    }
                }
            });
        };
        const mock = sinon.stub(myClient, 'on').callsFake(mockResponse);
        const myUser = new User(myClient, myDb);
        it('deletes message if profane', () => {
            expect(msgDelete.calledOnce).to.be.true;
        });
        it('notifies author that message was profane', () => {
            expect(notifyAuthor.calledOnce).to.be.true;
            expect(notifyAuthor.args[0][0]).to.equal('Your message has been removed for profanity.  If you believe this to be in error, contact an admin.  If this occurs repeatedly you may be banned.');
        });
    });
})