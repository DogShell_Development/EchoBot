// Import Config
const config = require('../config.json');

// Import Libs
import * as Discord from 'discord.js';
import * as moment from 'moment';
import * as mysql from 'mysql';

// Import Helpers
import { Uptime } from './helpers/uptime';

// Import Modules
import { Admin } from './modules/admin';
import { FAQ } from './modules/faq';
import { Logger } from './modules/logger';
import { User } from './modules/user';

// Create Client
const bot = new Discord.Client();

// Connect to mySQL
const db = mysql.createConnection({
    database: config.sql_db,
    host: config.sql_host,
    password: config.sql_password,
    user: config.sql_user,
});

const dbStatus = {
    error: '',
    isConnected: false,
};

// Create Shared Class Instances
const uptime = new Uptime();

// Test DB Connection
const setStartDb = (): Promise<void> => {
    return new Promise((resolve) => {
        const now = moment().format();
        db.query('UPDATE bot_tools SET last_login=? WHERE id=0;', [now], (err) => {
            if (err) {
                dbStatus.error = err.toString();
            } else {
                uptime.setStartTime(now);
                dbStatus.isConnected = true;
            }
            resolve();
        });
    });
};

// Check for Token
if (config && config.bot_token) {
    setStartDb().then(() => {
        bot.login(config.bot_token)
            .then(() => {
                // Log Bot Status
                console.log('\x1b[32m%s\x1b[0m', 'EchoBot Mk.2 successfully LOGGED IN!');
                // Log SQL Status
                if (dbStatus.isConnected) {
                    console.log('\x1b[32m%s\x1b[0m', 'DB Successfully Connected!');
                } else {
                    console.log('\x1b[41m%s\x1b[0m', 'ERR:', `\x1b[31mDB NOT CONNECTED WITH ERROR ${dbStatus.error}\x1b[0m`);
                }
                // Load Modules
                new Logger(bot);
                new Admin(bot, uptime);
                new FAQ(bot);
                new User(bot, db);
            })
            .catch((err) => {
                // Log Bot Status
                throw new Error(`EchoBot Failed to load with error: ${err}`);
            });
    });
} else {
    throw new Error('ERR! No config file or token found!');
}
