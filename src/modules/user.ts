import { Client } from 'discord.js';
import { Connection } from 'mysql';

import { customEmotes, isBotMessage } from '../helpers/helpers';

export class User {
    constructor(botInstance: Client, db: Connection) {
        this._profanityFilter(botInstance);
        this._userLookup(botInstance, db);
    }

    private _profanityFilter(discordBot: Client) {
        discordBot.on('message', (message) => {
            const profanityRegex = /(^|\s)nigg(a|r|er)($|\s)|(^|\s)fag($|\s)|(^|\s)autis(t|m)($|\s)|(^|\s)faggot($|\s)|(^|\s)retard($|\s)|(^|\s)cunt($|\s)|(^|\s)rap(e|ed)($|\s)|(^|\s)nazi($|\s)/i;
            const isClean = (profanityRegex.exec(message.content) === null);
            if (!isClean) {
                message.delete()
                    .then(() => { console.log(`Deleted message from ${message.author.username} due to profanity`); })
                    .catch(() => { console.log(`Failed to delete message from ${message.author.username} due to profanity (PLEASE CHECK PERMISSIONS)`); });
                message.author.createDM()
                    .then((channel) => {
                        channel.send('Your message has been removed for profanity.  If you believe this to be in error, contact an admin.  If this occurs repeatedly you may be banned.');
                    })
                    .catch((err) => {
                        console.log(`Unable to send DM to ${message.author.username} in regards to message deletion`);
                    });
            }
        });
    }

    private _userLookup(discordBot: Client, db: Connection): void {
        discordBot.on('message', (message) => {
            if (!message.content.indexOf('!lookup') && !isBotMessage(message)) {
                const discordID = message.content.match(/!lookup\s<@!?(\d+)/);
                if (discordID) {
                    db.query('SELECT id, username FROM users WHERE discord_id=?', [discordID[1]], (err, data) => {
                        if (!err && data.length > 0) {
                            message.channel.send(`${customEmotes.DogShellDev}\n<@${discordID[1]}>'s EchoLeague name is **${data[0].username}**\nTheir EL.gg profile can be found here: https://echoleague.gg/user/${data[0].id}`);
                        } else {
                            message.channel.send(`${customEmotes.LizardKing} Oh no!  Could not find account for <@${discordID[1]}>. The database is down or that account isn't linked`);
                        }
                    });
                } else {
                    message.channel.send('Invalid format, please use `!lookup @Username` to lookup a user by discord id');
                }
            }
        });
    }
}
