import { Client } from 'discord.js';

import { isBotMessage } from '../helpers/helpers';

export class FAQ {
    constructor(botInstance: Client) {
        this._faq(botInstance);
    }

    private _faq(discordBot: Client): void {
        discordBot.on('message', (message) => {
            if (message.content === 'ping' && !isBotMessage(message)) {
                message.channel.send('pong');
            }
        });
    }
}
