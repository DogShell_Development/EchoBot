import { Client, Message } from 'discord.js';
import * as moment from 'moment';

import { isAdmin, roleIds } from '../helpers/helpers';
import { Uptime } from '../helpers/uptime';

export class Admin {
    constructor(botInstance: Client, uptime: Uptime) {
        this._adminSwitch(botInstance, uptime);
    }

    private _adminSwitch(discordBot: Client, uptime: Uptime): void {
        discordBot.on('message', (message) => {
            if (isAdmin(message)) {
                this._checkUptime(message, uptime);
                this._muteUser(message);
                this._unmuteUser(message);
                this._botmuteUser(message);
                this._unbotmuteUser(message);
            }
        });
    }

    private _checkUptime(message: Message, uptime: Uptime): void {
        if (!message.content.indexOf('!uptime')) {
            const startTime = uptime.getStartTime() ? moment(uptime.getStartTime()).format('MMM, Do YYYY hh:mm a ZZ') : 'Failed to fetch start time :(';
            const timeOnline = uptime.getUptime();
            message.channel.send(`__Echobot Current Stats__\n**Current UpTime:** ${timeOnline} hours\n**Last Started: ** ${startTime}`);
        }
    }

    private _muteUser(message: Message): void {
        if (!message.content.indexOf('!mute')) {
            const discordID = message.content.match(/!mute\s<@!?(\d+)/);
            if (discordID) {
                message.guild.fetchMember(discordID[1])
                    .then((member) => {
                        member.addRole(roleIds.ChatMute, `User chat-muted by ${message.member.displayName}`);
                    })
                    .catch(() => {
                        message.author.send('Error Muting User, Invalid ID');
                    });
            }
        }
    }

    private _unmuteUser(message: Message): void {
        if (!message.content.indexOf('!unmute')) {
            const discordID = message.content.match(/!unmute\s<@!?(\d+)/);
            if (discordID) {
                message.guild.fetchMember(discordID[1])
                    .then((member) => {
                        member.removeRole(roleIds.ChatMute, `User chat-unmuted by ${message.member.displayName}`);
                    })
                    .catch(() => {
                        message.author.send('Error Unmuting User, Invalid ID');
                    });
            }
        }
    }

    private _botmuteUser(message: Message): void {
        if (!message.content.indexOf('!botmute')) {
            const discordID = message.content.match(/!botmute\s<@!?(\d+)/);
            if (discordID) {
                message.guild.fetchMember(discordID[1])
                    .then((member) => {
                        member.addRole(roleIds.BotMute, `User bot-muted by ${message.member.displayName}`);
                    })
                    .catch((err) => {
                        message.author.send('Error Bot-muting User, Invalid ID');
                    });
            }
        }
    }

    private _unbotmuteUser(message: Message): void {
        if (!message.content.indexOf('!unbotmute')) {
            const discordID = message.content.match(/!unbotmute\s<@!?(\d+)/);
            if (discordID) {
                message.guild.fetchMember(discordID[1])
                    .then((member) => {
                        member.removeRole(roleIds.BotMute, `User bot-unmuted by ${message.member.displayName}`);
                    })
                    .catch(() => {
                        message.author.send('Error Bot-Unmuting User, Invalid ID');
                    });
            }
        }
    }
}
