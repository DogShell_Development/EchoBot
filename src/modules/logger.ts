import { Client } from 'discord.js';

import { isBotMessage } from '../helpers/helpers';

export class Logger {
    constructor(botInstance: Client) {
        this._logger(botInstance);
    }

    private _logger(discordBot: Client): void {
        discordBot.on('message', (message) => {
            if (!isBotMessage(message)) {
                console.log(`Message from ${message.author.username}(${message.author.id}) at ${Date.now()}:\n=======> ${message.content}`);
            }
        });
    }
}
