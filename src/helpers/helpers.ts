import { Message } from 'discord.js';

export const isBotMessage = (message: Message) => {
    if (message.author.id === '420422150726615040') {
        return true;
    }

    return false;
};

export const isAdmin = (message: Message) => {
    return message.member.roles.exists('id', this.roleIds.Admin);
};

export const customEmotes = {
    DogShell: '<:DogShell:265377106161172490>',
    DogShellDev: '<:DogShell:248709355313037312>',
    LizardKing: '<:lizardking:306882721966456833>',
};

export const roleIds = {
    Admin: '261707169353826314',
    Admin_Dev: '271479255408312320',
    BotMute: '301742078528978944',
    Caster: '269272020884258816',
    ChatMute: '306324669466935308',
    ChatMute_Dev: '423328858494861312',
    EmoteMute: '301376492384813056',
    Everyone: '261386843923283970',
    ImageMute: '276520797642686464',
    LinkMute: '276520797642686464',
    TO_EU: '312320000079953921',
    TO_NA: '312319903090606082',
    TO_SEA: '312320069566988290',
    VoiceMute: '269903924381614080',
};
