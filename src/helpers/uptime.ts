import * as moment from 'moment';

export class Uptime {
    private startTime: string;

    public setStartTime(startTime: string): void {
        this.startTime = startTime;
    }

    public getStartTime(): string {
        return this.startTime;
    }

    public getUptime(): string {
        if (this.startTime) {
            const currentTime = moment();
            const diffHours = moment.duration(currentTime.diff(this.startTime)).as('hours').toString();

            return diffHours;
        }
        return 'No uptime data available! Likely MySQL Error';
    }
}
