# EchoBot Contribution Guide

Below is a list of rules & guidelines to help guide you in contributing to the **EchoBot** Development Project

-----

## Typescript

**EchoBot** is written in TypeScript.  All new modules/files should be `.ts` files, and follow basic typescript guidelines.  Declare types where needed, and create interfaces if you feel them to be necessary.

## Documentation & Commenting

All methods within a module should have at least a basic [JSDoc](http://usejsdoc.org/) on them.  This helps IDEs give descriptive information when autocompleting methods, and other developers to have good idea of what your methods are doing.

Please leave additional comments to help explain things as you see fit.

## Commit Structure

Please try to leave descriptive commit messages.  Avoid doing single-line (`git commit -m`) commits and use a text editor to provide more detail with the following structure:

```bash
Commit Subject Message (Try to keep < 50 char)

* Commit info 1
* Commit info 2
* Commit info 3
```

## Tests & Linting

The **EchoBot** repo is currently setup for tests with Chai, Mocha, and Sinon at this time.  Tests can be run from CLI using `npm test`.

Every module or helper file made should have a matching test file with it in the `/test/` folder.  All tests should match the name of their partner component, and end with `-spec.ts`.  All tests must pass for CI to clear on Gitlab.

There is currently no coverage minimum set at this time, but please do your best to provide a good general coverage.  Coverage can be run from CLI using `npm run coverage`.

All files are linted using TSLint.  You can check the rules for linting in the repo's [`tslint.json`](https://gitlab.com/DogShell_Development/EchoBot/blob/master/tslint.json) file.  All non-test files must pass linting for CI to clear on Gitlab.

## Modified Git Flow

Work on this project should never be done directly in the master branch, and should always be done in it's own branch.  All **features**, **bugfixes**, and **chores/tasks** (that require their own standalone work) should have their own tickets/issues assigned to them.  For **hotfixes** descriptions can be provided in the commits and MR details.  If you are chasing a new bug/feature/chore and there is no ticket, please make one to track the work.

Branches should follow the following naming scheme:

Issue Type | Branch Naming Format
---        | ---
Feature    | `feature/<ticket number>`
Bug        | `bug/<ticket number>`
Chore      | `chore/<ticket number>`
Hotfix     | `hotfix/<ticket number>` or `hotfix/<short fix description>`
-----

## Code Review

When work in a branch is completed and finished, a [new merge request](https://gitlab.com/DogShell_Development/EchoBot/merge_requests/new) should be opened for review.  Merge requests descriptions should follow the following format:

```markdown
**Ticket Addressed:** (#23/#4/#16)

**Fix Type:** (Major/Minor/Patch)

**Description:** (Description of fixes/changes/new features)
```

Most new features should be **minor** fixes, most **bugfixes** will be patches, and **major** changes should be reserved for major releases or any MRs containing breaking changes for other services in our ecosystem.

Assign **at least** one of the following members as reviewers:

* [Alex Muench](https://gitlab.com/ammuench)
* [Mags](https://gitlab.com/MichaelMagnoli)
* [Thomas Kosiewski](https://gitlab.com/ThomasK33)

And then ping the `#code-review` Discord channel with a link to the merge request.

Once your merge request is accepted, please complete the merge, and then tag your new change using the `npm version <patch/minor/major>` CLI command.  Then push your tag using `git push --tags`.